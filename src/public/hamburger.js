// On récupère l'ID de nos éléments du hamburger et de notre navMenu
const hamburger = document.getElementById("hamburger_mobile");
const navMenu = document.getElementById("menu");

// Activation
hamburger.addEventListener("click", () => {
    hamburger.classList.toggle("active");
    navMenu.classList.toggle("active");
});


