import { Application } from "express";
import AuthorController from "./controllers/AuthorController";
import CitationController from "./controllers/CitationController";

export default function route(app: Application)
{
    /** Static pages for Citations**/
    app.get('/', (req, res) =>
    {
        CitationController.index(req, res);
    });

    app.get('/citations-all/:page', (req, res) =>
    {
        CitationController.indexAll(req, res)
    });

    app.get('/citation-create', (req, res) =>
    {
        CitationController.showForm(req, res)
    });

    app.post('/citation-create', (req, res) =>
    {
        CitationController.create(req, res)
    });

    app.get('/citation-read/:id', (req, res) =>
    {
        
        CitationController.read(req, res)
    });

    app.get('/citation-update/:id', (req, res) =>
    {
        CitationController.showFormUpdate(req, res)
    });

    app.post('/citation-update/:id', (req, res) =>
    {
        CitationController.update(req, res)
    });

    app.get('/citation-delete/:id', (req, res) =>
    {
        CitationController.delete(req, res)
    });

    /** Static Pages for Client **/

    app.get('/backoffice', (req, res) =>
    {
        AuthorController.index(req, res)
    });

    app.get('/authors-all', (req, res) =>
    {
        AuthorController.indexAll(req, res)
    });

    app.get('/author-create', (req, res) =>
    {
        AuthorController.showForm(req, res)
    });

    app.post('/author-create', (req, res) =>
    {
        AuthorController.create(req, res)
    });

    app.get('/author-read/:id', (req, res) =>
    {
        AuthorController.read(req, res)
    });

    app.get('/author-update/:id', (req, res) =>
    {
        AuthorController.showFormUpdate(req, res)
    });

    app.post('/author-update/:id', (req, res) =>
    {
        AuthorController.update(req, res)
    });

    app.get('/author-delete/:id', (req, res) =>
    {
        AuthorController.delete(req, res)
    });
}
