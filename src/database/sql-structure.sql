-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Nicolas RAYNAUD
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2022-02-21 16:44
-- Created:       2022-02-21 09:42
PRAGMA foreign_keys = OFF;

-- Schema: mydb
--ATTACH "mydb.sdb" AS "mydb";
BEGIN;
CREATE TABLE "authors"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "name" VARCHAR(45) NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "publish" BOOLEAN NOT NULL DEFAULT 0
);
CREATE TABLE "citations"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "citation_desc" VARCHAR(150) NOT NULL,
  "authors_id" INTEGER NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT "fk_citations_authors"
    FOREIGN KEY("authors_id")
    REFERENCES "authors"("id")
);
CREATE INDEX "citations.fk_citations_authors_idx" ON "citations" ("authors_id");
COMMIT;
