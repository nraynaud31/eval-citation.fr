import { Request, Response } from "express-serve-static-core";

export default class CitationController {

    /**
     * Page index d'ajout de la dernière citation
     * @param req Request
     * @param res Response
     */
    static index(req: Request, res: Response) {
        const db = req.app.locals.db;

        const citations = db.prepare('SELECT * FROM citations INNER JOIN authors ON citations.authors_id = authors.id ORDER BY created_at DESC').get();

        res.render('pages/index', {
            title: 'Voici la dernière citation',
            citations: citations,
        });
    }

    /**
     * Récupère la liste des citations pour la page citation-all avec les paginations
     * @param req Request
     * @param res Response
     */
    static indexAll(req: Request, res: Response) {
        const db = req.app.locals.db;
        // On compte le nombre de citations dans la table
        const table = db.prepare("SELECT count (*) FROM citations").all();
        // On calcule le nombre total de pages
        const table_parse = Number(Math.round(table[0]['count (*)'] / 10))

        // On fait un array pour pouvoir proposer un nombre de pages dynamiques
        let tableau_count:any = []
        let i = 0
        while(i<table_parse){
            i++
            tableau_count.push(i)
        }   

        const citations = db.prepare('SELECT * FROM citations INNER JOIN authors ON citations.authors_id = authors.id LIMIT 10 OFFSET ?').all((Number(req.params.page)-1)*10);
        

        res.render('pages/index-all', {
            title: 'Voici la liste des citations',
            citations: citations,
            tableau_count: tableau_count,
        });
    }

    /**
     * Affiche le formulaire de création de la citation
     * @param req Request
     * @param res Response
     */
    static showForm(req: Request, res: Response): void {
        const db = req.app.locals.db;

        const authors = db.prepare("SELECT * FROM authors").all()

        res.render('pages/citation-create', {
            authors: authors,
        });
    }

    /**
     * Récupère le formulaire et insère la citation en db
     * @param req Request
     * @param res Response
     */
    static create(req: Request, res: Response): void {
        const db = req.app.locals.db;

        db.prepare('INSERT INTO citations ("citation_desc", "authors_id") VALUES (?, ?)').run(req.body.title, req.body.id_client);
        let addTrueAsPublish = db.prepare('UPDATE authors SET publish=1 WHERE id=?').run(req.body.id_client);

        console.log(req.body);

        CitationController.index(req, res);
    }

    /**
     * Affiche la Citation
     * @param req Request
     * @param res Response
     */
    static read(req: Request, res: Response): void {
        const db = req.app.locals.db;

        const citations = db.prepare('SELECT * FROM citations WHERE id = ?').get(req.params.id);

        res.render('pages/citation-read', {
            citations: citations
        });
    }

    /**
     * Affiche le formulaire pour modifier une citation
     * @param req Request
     * @param res Response
     */
    static showFormUpdate(req: Request, res: Response) {
        const db = req.app.locals.db;

        const citation = db.prepare('SELECT * FROM citations WHERE id = ?').get(req.params.id);
        const authors = db.prepare("SELECT * FROM authors").all();
        res.render('pages/citation-update', {
            citation: citation,
            authors: authors
        });
    }

    /**
     * Récupère le formulaire de la citation modifiée et l'ajoute dans la db
     * @param req Request
     * @param res Response
     */
    static update(req: Request, res: Response) {
        const db = req.app.locals.db;

        db.prepare('UPDATE citations SET citation_desc = ?, authors_id = ? WHERE id = ?').run(req.body.title, req.body.id_client, req.params.id);

        CitationController.index(req, res);
    }

    /**
     * Supprime une citation
     * @param req Request
     * @param res Response
     */
    static delete(req: Request, res: Response) {
        const db = req.app.locals.db;

        db.prepare('DELETE FROM citations WHERE id = ?').run(req.params.id);

        CitationController.index(req, res);
    }
}