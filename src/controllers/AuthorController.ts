import { Request, Response } from "express-serve-static-core";

export default class AuthorController {

    /**
     * Affiche la liste des authors & citations pour le Backoffice
     * @param req Request
     * @param res Response
     */
    static index(req: Request, res: Response) {
        const db = req.app.locals.db;

        const authors = db.prepare('SELECT * FROM authors').all();
        const citations = db.prepare("SELECT * FROM citations").all()

        res.render('pages/backoffice', {
            title: '',
            authors: authors,
            citations: citations,
        });
    }

    /**
     * Affiche la liste des authors
     * @param req Request
     * @param res Response
     */
     static indexAll(req: Request, res: Response) {
        const db = req.app.locals.db;

        const authors = db.prepare('SELECT * FROM authors WHERE publish=1').all();

        res.render('pages/author-all', {
            title: '',
            authors: authors,
        });
     }

    /**
     * Affiche le formulaire de création de l'author
     * @param req Request
     * @param res Response
     */
    static showForm(req: Request, res: Response): void {
        res.render('pages/author-create');
    }

    /**
     * Récupère le formulaire et insère l'author en db
     * @param req Request
     * @param res Response
     */
    static create(req: Request, res: Response): void {
        const db = req.app.locals.db;

        db.prepare('INSERT INTO authors ("name") VALUES (?)').run(req.body.title);

        AuthorController.index(req, res);
    }

    /**
     * Affiche 1es authors
     * @param req Request
     * @param res Response
     */
    static read(req: Request, res: Response): void {
        const db = req.app.locals.db;

        const authors = db.prepare('SELECT * FROM authors WHERE id = ?').get(req.params.id);
        const citations = db.prepare("SELECT * FROM citations WHERE authors_id = ?").all(req.params.id)
        
        res.render('pages/author-read', {
            authors: authors,
            citations: citations,
        });
    }

    /**
     * Affiche le formulaire pour modifier un author
     * @param req Request
     * @param res Response
     */
    static showFormUpdate(req: Request, res: Response) {
        const db = req.app.locals.db;

        const author = db.prepare('SELECT * FROM authors WHERE id = ?').get(req.params.id);

        res.render('pages/author-update', {
            author: author
        });
    }

    /**
     * Recupere le formulaire de l'author modifié et l'ajoute a la database
     * @param req Request
     * @param res Response
     */
    static update(req: Request, res: Response) {
        const db = req.app.locals.db;

        db.prepare('UPDATE authors SET name = ? WHERE id = ?').run(req.body.title, req.params.id);

        AuthorController.index(req, res);
    }

    /**
     * Supprime un author
     * @param req Request
     * @param res Response
     */
    static delete(req: Request, res: Response) {
        const db = req.app.locals.db;

        db.prepare('DELETE FROM authors WHERE id = ?').run(req.params.id);

        AuthorController.index(req, res);
    }
}